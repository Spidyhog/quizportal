import datetime
import json
from datetime import timedelta
import random

from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import JsonResponse
from django.shortcuts import render,redirect,HttpResponse
from .models import OnGoingcontest, Participation, Quiz, Ques_ans, QuizScore
from django.utils import timezone

from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.template.loader import render_to_string
from .tokens import account_activation_token
from django.core.mail import EmailMessage
from rest_framework.decorators import api_view

def sendhome(request):
    return redirect("/home/")

def home(request):
    return render(request,'quiz/login.html',{})

def rules(request):
    return render(request,'quiz/rules.html',{})

@login_required
def scorecard(request):
    try:
        curr=OnGoingcontest.objects.get(pk=1)
        user=Participation.objects.get(contest_part=curr.on_going,username=request.user)
        quesScore1 = QuizScore.objects.filter(participation=user, user_id=request.user,solved=0)
        quesScore2 = QuizScore.objects.filter(participation=user, user_id=request.user, solved=1)
        return render(request, "quiz/scorecard.html", {"message":'''
        Thanks for taking part in the quiz. You have correctly solved <strong>''' + str(quesScore2.count()) +''' </strong> out of <strong>
        '''+str(quesScore1.count()+quesScore2.count()) +'''</strong>'''})
    except:
        return render(request,"quiz/scorecard.html",{"message":"Sorry something went wrong try again later."})

@login_required
def contest_page(request):
    try:
        curr=OnGoingcontest.objects.get(pk=1)
        if (curr.start_time < timezone.now()) and (timezone.now() < curr.end_time):
            select_quiz_id=random.randint(1,len(curr.on_going.quiz_ids))
            select_quiz_obj=Quiz.objects.get(id=curr.on_going.quiz_ids[select_quiz_id-1])
            user,exists=Participation.objects.get_or_create(username=request.user,contest_part=curr.on_going)
            if exists:
                user.contest_start_time=timezone.now()
                user.quiz_allot = select_quiz_obj
                user.save()
                return redirect("/rules/")
            # print(user.contest_start_time-timedelta(hours=6,minutes=30) ," ",curr.start_time-timedelta(hours=6,minutes=30) )
            # print("Time after which user started the contest",((user.contest_start_time -curr.start_time)))
            # print("time at which his contest will end",(user.contest_start_time + timedelta(minutes=30))-timedelta(hours=6,minutes=30))
            # print("Contest end time ",curr.end_time-timedelta(hours=6,minutes=30))
            user_endtime=user.contest_start_time + timedelta(hours=1,minutes=35)
            #print("Current time",user_endtime-timedelta(hours=6,minutes=30))
            if (user.contest_start_time > curr.start_time) and   (timezone.now() < curr.end_time):
                    if user_endtime > timezone.now():
                        #print(user.quiz_allot)
                        all_question = Ques_ans.objects.filter(pk__in=user.quiz_allot.ques_ids).values_list("pk", "question", "option1","option2", "option3","option4")
                        attempt= QuizScore.objects.filter(user_id=request.user,participation=user).values_list("ques_id","selected")
                        #print(attempt)
                        sendtime=user_endtime - timezone.now()
                        #print(sendtime)
                        sendtime=str(sendtime).split(":")
                        if int(sendtime[0])==1:
                            sendtime[1]=str(int(sendtime[1])+60)
                        return render(request, 'quiz/ques.html',{"questions": all_question, "total": all_question.count(),"timeleft":sendtime[1]+":"+sendtime[2].split(".")[0],"attempts":attempt})
            return render(request,"quiz/scorecard.html",{"message":"Your time has finished thanks for attending the quiz result will be announced soon you can see your score card from + button on bottom side ."})
        else:
            if  (curr.start_time > timezone.now()):
                return render(request,"quiz/scorecard.html",{"message":"Sorry the contest will start soon."})
            if (curr.end_time < timezone.now()):
                return render(request,"quiz/scorecard.html",{"message":"Sorry no contest is live visit back later."})
    except Exception as e:
        return render(request,"quiz/scorecard.html",{"message":"Sorry no contest is live visit back later."}+e)
    return render(request, 'quiz/question.html', {"userhere": request.user})

@login_required
def ques_page(request,quesno):
    curr = OnGoingcontest.objects.filter(pk=1).exists()
    if curr:
        try:
            curr = OnGoingcontest.objects.get(pk=1)
            user_playing=Participation.objects.get(contest_part=curr.on_going,username=request.user)
            if quesno in user_playing.quiz_allot.ques_ids:
                ques=Ques_ans.objects.get(pk=int(quesno))
                ques_ids = user_playing.quiz_allot.ques_ids
                all_question = Ques_ans.objects.filter(pk__in=ques_ids).values_list("pk","question","option1","option2","option3","option4")
                return render(request,'quiz/ques.html',{"questions":all_question,"total":all_question.count()})
            else:
                return HttpResponse("Sorry login and continue again.")
        except Exception as e:
            return HttpResponse("Login and try again.")
    else:
        return HttpResponse("Sorry No On-going contest.")


def login_user(request):
    if not request.user.is_anonymous:
        logout(request)
        print("user logged out")
    curr = OnGoingcontest.objects.filter(pk=1).exists()
    if not curr:
        return  render(request,"quiz/scorecard.html",{"message":"Sorry No On-going contest."})
    else:
        curr = OnGoingcontest.objects.get(pk=1)
        user=request.POST.get("email")
        password=request.POST.get("password")
        print(request.POST)
        if not User.objects.filter(username=user).exists():
            return  render(request,"quiz/scorecard.html",{"message":"Sorry User with this username does not exists."})
        user_obj=authenticate(username=user,password=password)
        if user_obj is None:
            return  render(request,"quiz/scorecard.html",{"message":"Wrong Password Click <a href='/home/'>Here</a> to try again."})
        if not user_obj.is_staff:
            return render(request, "quiz/scorecard.html",{"message": "Please Confirm  your email to login."})
        if curr.outisider_allowed:
            login(request,user_obj)
            return redirect("/contest/quiz/")
        elif user_obj.is_staff:
            login(request,user_obj)
            return redirect("/contest/quiz/")
        elif user_obj.is_staff==False and curr.outisider_allowed==False:
            return HttpResponse("Sorry you can't participate in this contest. This is only for the students of JUET.")
        else:
            return HttpResponse("Sorry you can't participate in this contest. This is only for the students of JUET.")

def register(request):
    if request.method=="POST":
        print(request.POST)
        password1 = request.POST.get("password1")
        password2 = request.POST.get("password2")
        name = request.POST.get("name")
        email = request.POST.get("email")
        enroll= request.POST.get("enroll")
        if name and password1 and email and enroll and password1==password2:
            try:
                user = User.objects.create_user(username=enroll, password=password1, email=email,first_name=name)
                user.is_active = False
                user.save()
                current_site = get_current_site(request)
                mail_subject = 'Activate your blog account.'
                message = render_to_string('acc_active_email.html', {'user': user, 'domain': current_site.domain,
                    'uid': str(urlsafe_base64_encode(force_bytes(user.pk))), 'token': account_activation_token.make_token(user), })
                email = EmailMessage(mail_subject, message, to=[email])
                email.send()
                return  render(request,"quiz/scorecard.html",{"message":"Email-Verification code has been sent to your mail. Please veriy and then Login to continue."})
            except:
                return  render(request,"quiz/scorecard.html",{"message":"Username with this name already exists."})
        else:
            return  render(request,"quiz/scorecard.html",{"message":"Password should be same."})
    return  render(request,"quiz/register.html")

@login_required
def question(request):
    current=OnGoingcontest.objects.get(pk=1)
    participant_obj=Participation.objects.get(username=request.user,contest_part=current.on_going)
    ques_ids=participant_obj.quiz_allot.ques_ids
    all_question=Ques_ans.objects.filter(pk__in=ques_ids).values_list("pk")
    return JsonResponse({"question":"Hello"},safe=False)


@login_required
def submitans(request):
    curr=OnGoingcontest.objects.get(pk=1)
    user, exists = Participation.objects.get_or_create(username=request.user, contest_part=curr.on_going)
    user_endtime = user.contest_start_time + timedelta(minutes=95)
    if user_endtime > timezone.now():
        if request.method == "POST":
            question = request.POST.get("quesid")
            if question not in request.POST:
                return render(request, "quiz/scorecard.html",{"message": "Please don't play with items or you will be disqualified."})
            else:
                solution = request.POST.get(question)
                try:
                    ques = Ques_ans.objects.get(pk=question)
                    quiz_score, made = QuizScore.objects.get_or_create(participation=user, ques_id=ques,user_id=request.user)
                    quiz_score.selected = int(solution)
                    if int(ques.corr_ans) == int(solution):
                        quiz_score.solved = 1
                    if int(ques.corr_ans) != int(solution):
                        quiz_score.solved = 0
                    quiz_score.save()
                    return redirect("/contest/quiz/")
                except:
                    return render(request, "quiz/scorecard.html",{"message": "Sorry contest end click on the right bootom + button to see your score"})
    return render(request,"quiz/scorecard.html",{"message":"Sorry contest end click on the right bootom + button to see your score"})


def logoutuser(request):
    logout(request)
    return redirect("/home/")


def activate(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.is_staff=True
        user.save()
        # return redirect('home')
        return  render(request,"quiz/scorecard.html",{"message":"Thank you for your email confirmation. Now you can login your account. Click <a href='/home/'> Here</a> to login."})
    else:
        return render(request, "quiz/scorecard.html", {'message':'Activation link is invalid!'})

def endtest(request):
    try:
        curr=OnGoingcontest.objects.get(pk=1)
        user = Participation.objects.get(username=request.user, contest_part=curr.on_going)
        user.contest_start_time=user.contest_start_time-timedelta(hours=1,minutes=35)
        user.save()
        return redirect("/contest/quiz/")
    except:
        return render(request, "quiz/scorecard.html", {'message': "Something went wrong please login again."}+e)
